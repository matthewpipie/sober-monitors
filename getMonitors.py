import pandas as pd
import numpy as np

spreadsheet = pd.read_csv("sobers.csv")

needSobers=spreadsheet[spreadsheet['events'] == np.array(spreadsheet['events']).min()]

sampleNum = int(input("How many?\n"))

needSobers = needSobers.sample(min(sampleNum, len(needSobers)))

currentLevel = np.array(spreadsheet['events']).min()
while (len(needSobers) < sampleNum):
    currentLevel+=1
    sortaNeedSobers=spreadsheet[spreadsheet['events'] == currentLevel]
    sortaNeedSobers = sortaNeedSobers.sample(min(len(sortaNeedSobers), sampleNum - len(needSobers)))
    needSobers = needSobers.append(sortaNeedSobers, ignore_index=True)

print(needSobers)